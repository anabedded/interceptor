# README #

Interceptor is a tool that allows for high-performance, low latency, and low-resource input to output multiplexing. One single input can be streamed dynamically in real-time to multiple output channels.

### What is this repository for? ###

* This repository has the latest version of 'Interceptor' that was created for the Impinj Speedway Revolution reader.
* 1.0 [Production]

### How do I get set up? ###

* Compiling should be relatively straightforward, using the (included) Makefile.
* Look in conf/interceptor.conf for all configuration options.
* Interceptor depends upon the libltkcpp library provided by Impinj, to support the Speedway Revolution reader.
* Deployment instructions is provided below.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Deployment Instructions for the Speedway reader ###

To generate the upgrade file for the revolution reader (interceptor.upg):

* If this is a new build, update conf/cap_description.in with the updated build number.

* Update the files in cap/cust/interceptor/bin with the newest 'interceptor_arm' file from the latest build.

* Run the command from the root directory: cap_gen -d conf/cap_description.in -o interceptor.upg

### Who do I talk to? ###

* RVikram Seshadri (svikram@analytica-india.com)